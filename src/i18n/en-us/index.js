// This is just an example,
// so you can safely delete all default props below

export default {
  failed: 'Action failed',
  success: 'Action was successful',
   "downloads": "Downloads",
    "email":{
	"invalid": "Email Invalid"
    },
    "RegisterBeta" : {
	"title": "Register for Beta",
	"subtitle": "Mandatory only for iOS.",
	"appleid": "You must use your Apple Account Email to receive invitation."
    },
    "UnRegisterBeta" : {
	"title": "UnRegister for Beta",
	"subtitle": "Delete your email from our database",
	"rgpd": "Our API will delete your email, no backups."
    },
    "SendBug": {
	"title": "Send Feedback",
	"message":{
	    "label": "Bug Description",
	    "placeholder": "Please explain us more about the bug"
	}
    },
    "form": {
	"login":{
	    "label": "Login",
	    "placeholder": "Enter Your Login",
    "hint": "Enter the username you want to use",
    "rules": "Please type something"
	},
  "sendMsg": {
    "recipient": "Recipient username",
    "content": "Content of the message",
    "hintRecipient": "Enter the username of the recipient",
    "hintContent": "Type your message",
    "rulesRecipient": "Please type a username",
    "rulesContent": "Please type a message"
  },
	"email":{
	    "label": "Email",
	    "placeholder": "Enter Your Email"
	},
	"username": {
	    "label": "Username",
	    "placeholder": "Entrer your Username"
	},
	"phoneType": {
	    "label": "Phone Type",
	    "placeholder": "Choose your type of phone"
	},
	"upload": {
	    "label": "Drop your files here or click to upload",
	    "label-optional": "Drop your files here or click to upload\n(Optional)"
	},
	"submit": "Submit",
	"subscribeNews": "Subscribe to latest news",
	"optional": "Optional",
	"required": "Required"
    },
    "footer":
    {
	"copyrights": "MessEnkrypt Landing Page by",
	"license": {
	    "source_code": "This source code is licensed",
	    "CC": "The website content is licensed"
	},
	"cookie": "No cookies here, No analytics, just privacy."
    },
    "home": {
	"link": "Home",
	"title": "MessEnkrypt",
	"subtitle": "Privacy by design messaging application",
	"user-secret": "Privacy by Design",
	"eye-slash": "No one can see what you are doing",
	"key": "Every message are encrypted",
	"shield-lock": "All local content is encrypted"
    },
    "Bot": {
		"link": "Demo",
		"title": "Messaging Bot",
		"subtitle": "Messenkrypt Demo Bot",
		"receivedMessages": "Messages Received",
		"id": "id",
		"from": "from",
		"date": "date",
		"payload": "payload",
		"raw": "raw",
		"connected": "Connected",
		"disconnected": "Not connected",
		"sendMsg": ".. Or scan this QRCode and use the mobile application",
		"description": "Choose a username and start receiving encrypted messages",
      "sendMsgTitle": "Fill the form below to send encrypted messages",
      "userId": "Your user ID:",
		"lock": "Reset",
		"unlock": "Decrypt"
    },
    "downloads": {
	"title": "Downloads",
	"download": "Download",
	"ios": {
	    "title": "Register on Beta with Apple Account Email"
	},
	"android":
	{
	    "title": "You can download APK Here",
	    "latestAPK": "messenkrypt.apk"
	},
	"desktop": {
	    "title": "Desktop",
	    "subtitle": "Coming Soon..."
	}
    },
    "privacyPolicy": {
	"title": "MessEnkrypt",
	"subtitle": "Privacy Policy",
	"content": "test"
    },
    "about": {
	"link": "About"
    }
}
