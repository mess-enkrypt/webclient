import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state: {
      isLoading: false,
      userReadOnly: false,
      privkey: null,
      pubkey: null,
      messages: [],
    },
    mutations: {
      setIsLoading (state, isLoadingBool) {
        state.isLoading = isLoadingBool
      },
      setUserReadOnly (state, userReadOnlyBool) {
        state.userReadOnly = userReadOnlyBool
      },
      setPrivKey (state, key) {
        state.privkey = key
      },
      setPubKey (state, key) {
        state.pubkey = key
      },
      addMessage (state, message) {
        state.messages.push(message)
      },
      setMessageLocked (state, payload) {
        state.messages[payload.nth].locked = payload.value
      },
      setMessageMsg (state, payload) {
        state.messages[payload.nth].msg = payload.value
      },
      setMessageEncryptedMsg (state, payload) {
        state.messages[payload.nth].encryptedMsg = payload.value
      }
    },
    getters: {
    },
    actions: {
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
