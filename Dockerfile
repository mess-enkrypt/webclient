FROM node:12.14.0-alpine
WORKDIR /usr/src/app

#RUN npm config set registry http://172.42.42.42:4873

RUN apk update
RUN apk add python git

COPY package*.json ./

RUN npm install -g @quasar/cli
RUN npm i

COPY .env* ./


CMD ["quasar", "dev", "-m", "pwa"]
