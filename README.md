# MessEnkrypt Web App (messenkrypt-webapp)

WebApp to use MessEnkrypt

Requirements:
   - docker
   - docker-compose
   - make

##Don't forget to init, if not, will not works

- Init Project:
         ```make init```

- Launch attached in term:
         ```make dev```

- Launch:
        ```make start```

- Stop:
        ```make stop```

- Delete containers:
        ```make clean```

- Install docker (for *Nux users):
        ```make install_dc```

- Install docker-compose:
        ```m̀ake install_dcc```


## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
